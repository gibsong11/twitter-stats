package com.jackhenry.banno;

import com.vdurmont.emoji.EmojiParser;
import twitter4j.*;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

  private BlockingQueue<String> queue;
  private Stats stats;

  public Consumer(BlockingQueue<String> queue, Stats stats) {
    this.queue = queue;
    this.stats = stats;
  }

  @Override
  public void run() {

    System.out.println("started consumer client " + Thread.currentThread().getName());

    while(!Producer.shutdown) {
      String tweet = null;
      try {
        tweet = queue.take();
      } catch (InterruptedException e) {
        System.out.println(e.getMessage());
      }

      if (tweet != null) {
        try {
          Status status = TwitterObjectFactory.createStatus(tweet);//Throws if not "created" status. e.g. "deleted"
          stats.updateHashTagCount(status.getHashtagEntities());
          stats.updateDomainCount(status.getURLEntities());
          stats.updatePhotoCount(status.getMediaEntities().length);
          stats.updateEmojiCount(EmojiParser.extractEmojis(status.getText()));
        } catch (TwitterException e) {
          //ignore fails on all but "created" status, which is the only thing you are interested in analyzing.
        }
      }
    }
  }
}
