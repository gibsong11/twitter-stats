package com.jackhenry.banno;

import twitter4j.HashtagEntity;
import twitter4j.URLEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.toMap;

public class Stats {

  private final int TOP_RANKED = 10;
  private final String DECIMAL_FORMAT = "#0.00";
  private final double MINUTES_PER_HOUR = 60;
  private final double SECONDS_PER_MINUTE = 60;

  private final ConcurrentSkipListMap<String, AtomicInteger> hashTagCounts;
  private final ConcurrentSkipListMap<String, AtomicInteger> domainCounts;
  AtomicInteger photoCount = new AtomicInteger(0);
  private final ConcurrentSkipListMap<String, AtomicInteger> emojiCounts;


  private long startTime;

  public Stats(long startTime) {
    this.startTime = startTime;
    hashTagCounts = new ConcurrentSkipListMap<>();
    domainCounts = new ConcurrentSkipListMap<>();
    emojiCounts = new ConcurrentSkipListMap<>();
  }

  public void updateHashTagCount(HashtagEntity[] hashtagEntities) {
    for(HashtagEntity hashtagEntity : hashtagEntities) {
      updateCount(hashtagEntity.getText(), hashTagCounts);
    }
  }

  public void updateDomainCount(URLEntity[] urlEntities) {
    for(URLEntity urlEntity : urlEntities) {
      String domain = getHostName(urlEntity.getExpandedURL());
      if(domain != null) {
        updateCount(domain, domainCounts);
      } else {
        System.out.println("attempted to get host name from " + domain + " but failed.");
      }
    }
   }

  private String getHostName(String url) {
    try {
      URI uri = new URI(url);
      String domain = uri.getHost();
      return domain.startsWith("www.") ? domain.substring(4) : domain;
    } catch (URISyntaxException e) {
      System.out.println("unable to extract host name from: " + url);
    }
    return null;
  }

   public void updatePhotoCount(int count) {
    if(count > 0) {
      photoCount.addAndGet(count);
    }
  }

  public void updateEmojiCount(List<String> emojis) {
    for(String emoji : emojis) {
      updateCount(emoji, emojiCounts);
    }
  }

  private void updateCount(String key, ConcurrentMap<String, AtomicInteger> counts) {
    AtomicInteger currentCount = counts.get(key);
    if(currentCount == null) {
      currentCount = new AtomicInteger(0);
      AtomicInteger old = counts.putIfAbsent(key, currentCount);
      if (old != null) {
        currentCount = old;
      }
    }
    currentCount.incrementAndGet();
  }

  public void print(long totalTweets) {

    if(totalTweets > 0) {
      System.out.println("Twitter Statistics:");
      System.out.println("Total Tweets: " + totalTweets);
      printAverages(totalTweets);

      System.out.println("Photo Stats:");
      System.out.println("  Percentage: " + formatDecimal((((double)photoCount.intValue()/(double)totalTweets)* 100), DECIMAL_FORMAT)  + "%");

      System.out.println("Hash Tag Stats:");
      printTop(hashTagCounts);
      printPercentage(totalTweets, hashTagCounts);

      System.out.println("Domain Stats:");
      printTop(domainCounts);
      printPercentage(totalTweets, domainCounts);

      System.out.println("Emoji Stats:");
      printTop(emojiCounts);
      printPercentage(totalTweets, emojiCounts);
      System.out.println("--------------------------------------------------------------------------------");
    } else {
      System.out.println("Still waiting for tweets...");
    }
  }

  private void printAverages(long totalTweets) {
    long elapsedTime = System.currentTimeMillis() - startTime;
    long seconds = TimeUnit.MILLISECONDS.toSeconds(elapsedTime);
    if(seconds > 0) {
      double avgTweetsPerSec = (double)totalTweets/(double)seconds;
      double avgTweetsPerMin = (double)totalTweets * (double)(SECONDS_PER_MINUTE/seconds);
      double avgTweetsPerHour = (double)totalTweets * (double)(SECONDS_PER_MINUTE/seconds) * MINUTES_PER_HOUR;

      System.out.println("Avg Tweets/Second: " + formatDecimal(avgTweetsPerSec, DECIMAL_FORMAT));
      System.out.println("Avg Tweets/Minute: " + formatDecimal(avgTweetsPerMin, DECIMAL_FORMAT));
      System.out.println("Avg Tweets/Hour: " + formatDecimal(avgTweetsPerHour, DECIMAL_FORMAT));
    }
  }

  private void printTop(ConcurrentMap<String, AtomicInteger> counts) {
    System.out.println("  Top " + TOP_RANKED + ":");

    //collect as LinkedHashMap to preserve order
    Map sorted = counts.entrySet()
            .stream()
            .parallel()
            .sorted(Collections.reverseOrder(Comparator.comparingInt(e -> e.getValue().get())))
            .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

    sorted.entrySet().stream().limit(TOP_RANKED).forEach(e-> System.out.println("    " + e));
  }

  private void printPercentage(long totalTweets, ConcurrentMap<String, AtomicInteger> counts) {
    long sum = counts.values().stream().mapToInt(AtomicInteger::intValue).sum();
    double percentage = ((double)sum/(double)totalTweets) * 100;
    System.out.println("  Percentage: " + formatDecimal(percentage, DECIMAL_FORMAT) + "%");
  }

  private String formatDecimal(double number, String pattern) {
    NumberFormat formatter = new DecimalFormat(pattern);
    return formatter.format(number);
  }
}
