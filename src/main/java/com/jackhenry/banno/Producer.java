package com.jackhenry.banno;


import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesSampleEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.BasicClient;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Producer {

  static volatile boolean shutdown = false;
  static private final int NUM_CONSUMERS = 3;

  public static void run(String consumerKey, String consumerSecret, String token, String secret) throws InterruptedException {

    BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);

    StatusesSampleEndpoint endpoint = new StatusesSampleEndpoint();
    endpoint.stallWarnings(false);

    Authentication auth = new OAuth1(consumerKey, consumerSecret, token, secret);

    BasicClient client = new ClientBuilder()
            .name("sampleClient")
            .hosts(Constants.STREAM_HOST)
            .endpoint(endpoint)
            .authentication(auth)
            .processor(new StringDelimitedProcessor(queue))
            .build();


    client.connect();

    Stats stats = new Stats(System.currentTimeMillis());

    startConsumers(NUM_CONSUMERS, queue, stats);

    System.out.println("--------------------------------------------------------------------------------");
    while(!shutdown) {
      stats.print(client.getStatsTracker().getNumMessages());
      Thread.sleep(20000);  //print stats every 20 seconds
    }

    client.stop();
  }

  public static void startConsumers(int numConsumers, BlockingQueue<String> queue, Stats statsCounts) {
    for(int i=0; i < numConsumers; i++) {
      new Thread(new Consumer(queue, statsCounts)).start();
    }
  }

  public static void main(String[] args) {

    try {
      String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
      String appConfigPath = rootPath + "application.properties";

      Properties twitterOAuthProperties = new Properties();
      twitterOAuthProperties.load(new FileInputStream(appConfigPath));

      Producer.run(twitterOAuthProperties.getProperty("consumerKey"),
                   twitterOAuthProperties.getProperty("consumerSecret"),
                   twitterOAuthProperties.getProperty("token"),
                   twitterOAuthProperties.getProperty("secret"));
    } catch (InterruptedException e) {
      System.out.println(e.getMessage());
    } catch (FileNotFoundException e) {
      System.out.println("unable to load twitter application.properties file");
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }
}
